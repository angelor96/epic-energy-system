package energy.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.login.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
	
	Role findById(long id);

}
