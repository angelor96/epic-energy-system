package energy.system.repository;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import energy.system.model.login.User;

public interface UserRepository extends JpaRepository<User, Long>{
	
	User findById(long id);
	Page<User> findByName(String name, Pageable pageable);
	Page<User> findBySurname(String surname, Pageable pageable);
	//User findByUsername(String username);
	User findByEmail(String email);	
	Optional<User> findByUsername(String username);
}
