package energy.system.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long>{

	 Page<Customer> findByName(String name,Pageable pageable);
	 Page<Customer> findBySurname(String surname, Pageable pageable);
	 Customer findByEmail(String email);
	 Customer findByTelephone(String telephone);
}
