package energy.system.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.Province;

public interface ProvinceRepository extends JpaRepository<Province, Long>{

	Province findById(long id);
	//Province findProvinceByAcronym(Province province, String acronym);
	Optional<Province> findByAcronym(String acronym);
}
