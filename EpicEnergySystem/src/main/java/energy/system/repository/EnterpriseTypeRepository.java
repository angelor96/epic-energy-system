package energy.system.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.EnterpriseType;

public interface EnterpriseTypeRepository extends JpaRepository<EnterpriseType, Long>{

	Page<EnterpriseType> findByEnterpriseType(EnterpriseType enterpriseType, Pageable pageable);
	EnterpriseType findById(long id);
}
