package energy.system.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long>{

	 Address findByWayAndCivicAndPlace( String way, int civic,String place);
	 Page<Address> findByCAP(int cap, Pageable pageable);
	 Address findById(long id);
}
