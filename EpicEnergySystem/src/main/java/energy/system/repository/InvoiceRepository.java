package energy.system.repository;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.Enterprise;
import energy.system.model.Invoice;
import energy.system.model.InvoiceStatus;

public interface InvoiceRepository extends JpaRepository<Invoice, Long>{
	
	 Invoice findByNumber(int number);
	 Invoice findById(long id);
	 /*
	  * Ricerca di tutte le fatture per anno
	  */
	 Page<Invoice> findByYear(int year, Pageable pageable);
	 /*
	  * Ricerca di tutte le fatture per data di inserimento.
	  */
	 Page<Invoice> findByDate(Date date, Pageable pageable);
	 /*
	  * Ricerca delle fatture presenti in un range impostato dai due parametri inseriti.
	  */
	 Page<Invoice> findByTotalBetween(BigDecimal total,BigDecimal total2, Pageable pageable);
	 /*
	  * Ricerca di tutte le fatture tramite l'id della'impresa.
	  */
	 Page<Invoice> findAllByEnterpriseId( long id, Pageable pageable);
	 /*
	  * Ricerca di tutte le fatture ordinate per data.
	  */
	 Page<Invoice> findAllByOrderByDate(Pageable paging);
	 /*
	  * Ricerca di tutte le fatture presenti in un range impostato dai due parametri inseriti.
	  */
	 Page<Invoice> findByDateBetween(Date date,Date date2, Pageable pageable);
	 /*
	  * Ricerca di tutte le fatture mediante l'impresa.
	  */
	 Page<Invoice>  findAllByEnterprise(Enterprise enterprise, Pageable pageable);
	 /*
	  * Ricerca di tutte le fatture in base allo stato della medesima.
	  */
	 Page<Invoice>  findAllByInvoiceStatus(InvoiceStatus invoiceStatus, Pageable pageable);
}
