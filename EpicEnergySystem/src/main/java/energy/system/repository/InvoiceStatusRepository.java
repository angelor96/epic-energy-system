package energy.system.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.InvoiceStatus;

public interface InvoiceStatusRepository extends JpaRepository<InvoiceStatus, Long>{

	 InvoiceStatus findById(long id);
	 /*
	  * Ricerca delle fatture mediante lo stato della medesima.
	  */
	 Page<InvoiceStatus> findByInvoiceStatus(String invoiceStatus, Pageable pageable);
}
