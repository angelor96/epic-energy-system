package energy.system.repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import energy.system.model.Enterprise;
import energy.system.model.Invoice;

public interface EnterpriseRepository extends JpaRepository<Enterprise, Long>{
	
	public interface ImportPerYear {
        /**
         * @return il cliente
         */
        Enterprise getEnterprise();
        /**
         * @return il fatturato
         */
        BigDecimal getTotal();
    }

	 Enterprise findByName(String name);
	 Enterprise findByVAT(String VAT);
	 Enterprise findByEmail(String email);
	 Enterprise findByPEC(String PEC);
	 Enterprise findByTelephone(String telephone);
	 Enterprise findById(long id);	
	 
	 /*
	  * Ricerca di un'azienda tramite data di inserimento.
	  */
	 Page<Enterprise> findByInsertDateBetween(LocalDate insertDate, LocalDate insertDate2, Pageable pageable);
	 /*
	  * Ricerca di un'azienda tramite data di ultimo contatto.
	  */
	 Page<Enterprise> findByUltimateTouchBetween(Date ultimateTouch, Date ultimateTouch2, Pageable pageable);
	 /*
	  * Ricerca di tutte le aziende filtrate per la data di inserimento.
	  */
	 Page<Enterprise> findAllByInsertDate(LocalDate insertDate, Pageable pageable);
	 /*
	  * Ricerca di tutte le aziende ordinate per nome.
	  */
 	 Page<Enterprise> findAllByOrderByName( Pageable pageble);
 	 /*
 	  * Ricerca delle azienda ordinate per data di inserimento.
 	  */
 	 Page<Enterprise> findAllByOrderByInsertDate(Pageable pageable);
 	 /*
 	  * Ricerca di tutte le aziende tramite un parametro in input
 	  */
 	 Page<Enterprise> findByNameContains(String name, Pageable pageable);
 	 /*
 	  * Ricerca di tutte le aziende tramite data di ultimo contatto.
 	  */
   	 Page<Enterprise> findAllByOrderByUltimateTouch(Pageable pageable);
	/*
	 * Calcolo dell'importo del fatturato annuale dell'azienda
	 */
 	@Query("SELECT SUM(i.total) FROM Invoice i WHERE i.enterprise.id=:enterpriseId AND i.year=:year")
	BigDecimal findImportByYear(long enterpriseId, int year);
	 /*
	  * Ricerca dell'importo del fatturato aziendale di tutte le aziende con annesse specifice aziendali.
	  */
	@Query("SELECT (SELECT e FROM Enterprise e WHERE e.id=i.enterprise.id ) as enterprise, SUM(i.total)"
			+ " AS total FROM Invoice i WHERE  i.year=:year GROUP BY i.enterprise.id")
    List<ImportPerYear> findTotalByYear(int year);
	
	 

}
