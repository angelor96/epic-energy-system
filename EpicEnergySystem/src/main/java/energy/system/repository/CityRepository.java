package energy.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import energy.system.model.City;
import energy.system.model.Province;

public interface CityRepository extends JpaRepository<City, Long>{

	City findById(long id);
	
	City findByNameAndProvince(String name, Province province);	
	
	}
