package energy.system.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import energy.system.model.login.User;
import lombok.Data;

@Data
public class UserDetailsImpl implements UserDetails {
	
	private static final long serialVersionUID = -7130684040991684077L;
	
	private long id;
	private String username;
	@JsonIgnore
	private String password;
	
	// Proprietà dell'utente: abilitato, bloccato, scaduto, password scaduta
	// Locked: account sospeso automaticamente a causa di tentativi di accesso non validi
	// Expired: account disattivato amministrativamente in base a un timing (es. abbonamento scaduto)
	// CredentialsNonExpired: scadenza delle credenziali (password) 
	private boolean accountNonLocked = true;	
	private boolean accountNonExpired = false;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	private Date expirationTime;
	
	// GrantedAuthority, rappresenta un'autorizzazione concessa (letture, scrittura, ecc) 
	private Collection<? extends GrantedAuthority> authorities;

	
	// Costruttore
	public UserDetailsImpl(long l, String username, String nome, String password, Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = l;
		this.username = username;
		this.password = password;		
		this.accountNonLocked = enabled;
		this.accountNonExpired = enabled;
		this.credentialsNonExpired = enabled;
		this.authorities = authorities;
	}
	
	

	public static UserDetailsImpl build(User user) {
		// SimpleGrantedAuthority, implementazione concreta di base di una GrantedAuthority
		List<GrantedAuthority> authorities = user.getRole().stream().map(role -> new SimpleGrantedAuthority(role.getRoleType().name())).collect(Collectors.toList());	// Restituisce i dati dello user e la lista delle sue autorizzazioni
		return new UserDetailsImpl(user.getId(), user.getUsername(), user.getName(), user.getPassword(), authorities);
	}
}
