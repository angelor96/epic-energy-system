package energy.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.InvoiceStatus;
import energy.system.service.implementations.InvoiceStatusServiceImpl;

@RestController
@RequestMapping("/api/invoicestatus")
public class InvoiceStatusController {
	
	@Autowired
	InvoiceStatusServiceImpl iService;
	
	@PostMapping(value="/saveinvoicestatus")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> saveInvoiceStatus( @RequestBody InvoiceStatus invoicseStatus, HttpServletRequest request) {
	
		return ResponseEntity.ok(iService.save(invoicseStatus));
	}
	
	@PostMapping(value="/updateinvoicestatus")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateInvoiceStatus( @RequestBody InvoiceStatus invoicseStatus, HttpServletRequest request) {
	
		return ResponseEntity.ok(iService.update(invoicseStatus));
	}
	
	@DeleteMapping(value="/deleteinvoicestatus")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> deleteInvoiceStatus(@RequestParam long id, HttpServletRequest request) {
	
		var e= iService.delete(id);
		return ResponseEntity.ok("Delete executed!");
	}

}
