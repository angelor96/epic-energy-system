package energy.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.Customer;
import energy.system.service.implementations.CustomerServiceImpl;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
	
	@Autowired
	CustomerServiceImpl cService;
	
	@PostMapping(value="/savecustomer")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> saveCustomer( @RequestBody Customer customer) {
	
		return ResponseEntity.ok(cService.save(customer));
	}
	
	@PostMapping(value="/updatecustomer")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateCustomer( @RequestBody Customer customer) {
	
		return ResponseEntity.ok(cService.update(customer));
	}
	
	@DeleteMapping(value="/deletecustomer")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> deleteCustomer( @RequestParam long id) {
	
		var e= cService.delete(id);
		return ResponseEntity.ok("Delete executed!");
	}

}
