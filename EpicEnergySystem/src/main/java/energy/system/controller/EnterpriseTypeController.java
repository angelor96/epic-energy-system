package energy.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.EnterpriseType;
import energy.system.repository.EnterpriseTypeRepository;
import energy.system.service.implementations.EnterpriseTypeServiceImpl;
@RestController
@RequestMapping("/api/enterprisetype")
public class EnterpriseTypeController {
	
	@Autowired
	EnterpriseTypeServiceImpl eService;
	
	@Autowired
	EnterpriseTypeRepository enterRepo;
	
	@PostMapping(value="/saveenterprisetype")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> saveEnterpriseType( @RequestBody EnterpriseType enterpriseType) {
	
		return ResponseEntity.ok(eService.save(enterpriseType));
	}
	
	@PostMapping(value="/updateenterprisetype")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateEnterpriseType( @RequestBody EnterpriseType enterpriseType) {
	
		return ResponseEntity.ok(eService.update(enterpriseType));
	}
	
	@DeleteMapping(value="/deleteenterprisetype")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> deleteEnterpriseType(@RequestParam long id) {
	
		var e= eService.delete(id);
		return ResponseEntity.ok("Delete executed!");
	}

}
