package energy.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.Address;
import energy.system.security.JwtUtils;
import energy.system.service.implementations.AddressServiceImpl;
import energy.system.service.implementations.UserServiceImpl;

@RestController
@RequestMapping("/api/address")
public class AddressController {
	
	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	AddressServiceImpl aService;
	
	@Autowired
	UserServiceImpl uService;
	
	
	/**
	 * 
	 * @param nameCity      
	 * 
	 * @param acronym
	 * 
	 * @param customerId
	 * 
	 * @param address
	 * 
	 * @return  Salvataggio di un address
	 */
	 @PostMapping(value="/saveaddress/{nameCity}/{acronym}/{customerId}")
	    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	 public ResponseEntity<?> saveAddress(@PathVariable String nameCity,@PathVariable String acronym, @PathVariable long customerId,@RequestBody Address address) {
		 
			return ResponseEntity.ok(aService.save(nameCity, acronym, customerId, address));
	 }
	 
	 /**
	  * 										
	  * @param cityId
	  * 
	  * @param customerId
	  * 
	  * @param address
	  * 
	  * @return modifica di un address
	  */
	 @PostMapping(value="/updateaddress/{nameCity}/{acronym}/{customerId}")
	    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	 public ResponseEntity<?> updateAddress(@PathVariable String nameCity,@PathVariable String acronym, @PathVariable long customerId,@RequestBody Address address) {
		 
			return ResponseEntity.ok(aService.update(nameCity, acronym, customerId, address));
	 }
	 
	 /**
	  * 
	  * @param id  
	  * 
	  * @return cancellazione di un address
	  */
	 @DeleteMapping(value="/deleteaddress")
	    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	 public ResponseEntity<?> deleteAddress(@RequestParam long id) {
		
		 var e= aService.delete(id);
			return ResponseEntity.ok("Delete executed!");
	 }
	 
	 
	 
	 
	

}
