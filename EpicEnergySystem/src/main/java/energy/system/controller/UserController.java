package energy.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.login.User;
import energy.system.service.implementations.UserServiceImpl;

@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	UserServiceImpl uService;

	@PostMapping(value="/saveuser/{idRole}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> saveUser( @PathVariable long idRole,@RequestBody User user, HttpServletRequest request) {
	
		return ResponseEntity.ok(uService.save(idRole,user));
	}
	
	@PostMapping(value="/updateuser/{idRole}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateUser( @PathVariable long idRole,@RequestBody User user, HttpServletRequest request) {
	
		return ResponseEntity.ok(uService.update(idRole,user));
	}
	
	@DeleteMapping(value="/deleteuser")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> deleteUser( @RequestParam long id, HttpServletRequest request) {
	
		return ResponseEntity.ok(uService.delete(id));
	}
}
