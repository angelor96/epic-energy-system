package energy.system.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.Invoice;
import energy.system.service.implementations.InvoiceServiceImpl;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {
	
	@Autowired
	InvoiceServiceImpl iService;
	
	@PostMapping(value="/saveinvoice/{enterpriseId}/{invoiceStatusId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> saveInvoice(@PathVariable long enterpriseId, @PathVariable long invoiceStatusId, @RequestBody Invoice invoice, HttpServletRequest request) {
	
		return ResponseEntity.ok(iService.save(enterpriseId,invoiceStatusId,invoice));
	}
	
	@PostMapping(value="/updateinvoice/{enterpriseId}/{invoiceStatusId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateInvoice(@PathVariable long enterpriseId, @PathVariable long invoiceStatusId, @RequestBody Invoice invoice, HttpServletRequest request) {
	
		return ResponseEntity.ok(iService.update(enterpriseId,invoiceStatusId,invoice));
	}
	
	@DeleteMapping(value="/deleteinvoice")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> deleteInvoice(@RequestParam long id, HttpServletRequest request) {
	
		 var e= iService.delete(id);
			return ResponseEntity.ok("Delete executed!");
	}
	
	@GetMapping(value="/gettotalbyrange")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<Page<Invoice>> getByTotalBetween(@RequestParam BigDecimal total, 
			@RequestParam BigDecimal total2, @RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="4") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort ) {
	
		return ResponseEntity.ok(iService.getByTotalBetween(total,total2,page,size, direction, sort));
	}
	
	@GetMapping(value="/getbyyear")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<Page<Invoice>> getInvoiceByYear(@RequestParam int year, @RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="4") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort ) {

		return ResponseEntity.ok(iService.getByYear(year,page,size, direction, sort));
	}
	
	 @GetMapping(value = "/getorderedbyinsertdate")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public ResponseEntity<Page<Invoice>> getAllOrderedByDate(@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "date") String sort ){
			
			return ResponseEntity.ok( iService.getAllByOrderByDate( page, size, direction, sort));
	  }
	
	 @GetMapping(value="/getdatebyrange")
	    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
		public ResponseEntity<Page<Invoice>> getByDateBetween(@RequestParam String date, @RequestParam String date2, @RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="4") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort ) {
		
			return ResponseEntity.ok(iService.getByDateBetween(fromString(date),fromString(date2),page,size, direction, sort));
		}
	 
	 @GetMapping(value="/getinvoicebyenterpriseid/{enterpriseId}")
	    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
		public ResponseEntity<Page<Invoice>> getInvoicesOrderedByEnterpriseId(@PathVariable long enterpriseId, @RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="4") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort ) {
		
			return ResponseEntity.ok(iService.getInvoicesByOrderByEnterpriseId(enterpriseId,page,size, direction, sort));
		}
	 
	 @GetMapping(value="/getinvoicebyinvoicestatusid/{invoiceStatusId}")
	    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
		public ResponseEntity<Page<Invoice>> getInvoicesByOrderByInvoiceStatusId(@PathVariable long invoiceStatusId, @RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="4") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort ) {
		
			return ResponseEntity.ok(iService.getInvoicesByOrderByInvoiceStatusId(invoiceStatusId,page,size, direction, sort));
		}

	 Date fromString(String date) {
	        try {
	            // cerca di convertire la data
	            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
	        } catch (ParseException e) {
	            // se non va bene la conversione
	            // ritorna la data di oggi
	            return new Date();
	        }
	    }
}
