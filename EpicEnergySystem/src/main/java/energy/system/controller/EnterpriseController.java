package energy.system.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import energy.system.model.Enterprise;
import energy.system.repository.EnterpriseRepository.ImportPerYear;
import energy.system.service.implementations.EnterpriseServiceImpl;

@RestController
@RequestMapping("/api/enterprise")
public class EnterpriseController {
	

	
	@Autowired
	EnterpriseServiceImpl eService;
	
	@PostMapping(value="/saveenterprise/{customerId}/{enterpriseTypeId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> saveEnterprise(@PathVariable long customerId,@PathVariable long enterpriseTypeId, @RequestBody Enterprise enterprise, HttpServletRequest request) {
	
		return ResponseEntity.ok(eService.save(customerId, enterpriseTypeId,enterprise));
	}
	
	@PostMapping(value="/updateenterprise/{customerId}/{enterpriseTypeId}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> updateEnterprise(@PathVariable long customerId,@PathVariable long enterpriseTypeId,@RequestBody Enterprise enterprise, HttpServletRequest request) {
	
		return ResponseEntity.ok(eService.update(customerId,enterpriseTypeId,enterprise));
	}
	
	@DeleteMapping(value="/deleteenterprise")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public ResponseEntity<?> deleteEnterprise(@RequestParam long id, HttpServletRequest request) {
	
		var e= eService.delete(id);
		return ResponseEntity.ok("Delete executed!");
	}

	@GetMapping(value = "/getrangebyinsertdate")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Enterprise> getAllSortByInsertDateBetween(@RequestParam String insertDate,@RequestParam String insertDate2,@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort  ){
		Page<Enterprise> pag= eService.getByInsertDateBetween(LocalDate.parse(insertDate), LocalDate.parse(insertDate2), page, size, direction, sort);
		return pag;		
	}
	

	@GetMapping(value = "/getrangebyultimatetouch")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public Page<Enterprise> getAllSortUltimateTouchBetween(@RequestParam String ultimateTouch,@RequestParam String ultimateTouch2,@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort  ){
		Page<Enterprise> pag= eService.getByUltimateTouchBetween(fromString(ultimateTouch),fromString( ultimateTouch2), page, size, direction, sort);
		return pag;		
	}
	  
	   
	  @GetMapping(value = "/getorderedbyname")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public Page<Enterprise> getAllOrderedByName(@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "name") String sort ){
			Page<Enterprise> pag= eService.getAllOrderedByName( page, size, direction, sort);
			return pag;
	  }
	  
	  
	  @GetMapping(value = "/getorderedbyinsertdate")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public Page<Enterprise> getAllOrderedByInsertDate(@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "insertDate") String sort ){
			Page<Enterprise> pag= eService.getAllByOrderByInsertDate( page, size, direction, sort);
			return pag;
	  }
	  
	  @GetMapping(value = "/getimportperyear")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public ResponseEntity<?> getImportPerYear(@RequestParam long enterpriseId, @RequestParam int year ){
			
			return ResponseEntity.ok(eService.getImportPerYear( enterpriseId, year));
	  }
	  
	  
	  @GetMapping(value = "/getorderedbyultimatetouch")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public Page<Enterprise> getAllOrderedByUltimateTouch(@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "ultimateTouch") String sort ){
			Page<Enterprise> pag= eService.getAllOrderedByUltimateTouch( page, size, direction, sort);
			return pag;
	  }
	  
	  
	  @PostMapping(value = "/gettotalbyyear")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public ResponseEntity<List<ImportPerYear>> getTotalByYear( @RequestParam int year ){
			
			return ResponseEntity.ok(eService.getTotalByYear( year));
	  }
	  
	  @GetMapping(value = "/getbynamecontains")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	  public Page<Enterprise> getByNameContains(@RequestParam String name,@RequestParam(defaultValue="0") Integer page,@RequestParam(defaultValue="10") Integer size, @RequestParam(defaultValue = "asc") String direction,@RequestParam(defaultValue = "id") String sort ){
			Page<Enterprise> pag=eService.getByNameContains(name, page, size, direction, sort);
			return pag;
	  }
	  
	  
	 
	  Date fromString(String date) {
	        try {
	            // cerca di convertire la data
	            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
	        } catch (ParseException e) {
	            // se non va bene la conversione
	            // ritorna la data di oggi
	            return new Date();
	        }
	    }
	  
	  
}

