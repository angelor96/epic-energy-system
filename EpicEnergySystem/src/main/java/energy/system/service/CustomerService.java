package energy.system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.Customer;

public interface CustomerService {
	
	 Customer save(Customer customer);
	 Customer update(Customer customer);
	 Customer delete(long id);
	 Page<Customer> getByName(String name, Pageable pageable);
	 Page<Customer> getBySurname(String surname, Pageable pageable);
	 Customer getByEmail(String email);
	 Customer getByTelephone(String telephone);
	

}
