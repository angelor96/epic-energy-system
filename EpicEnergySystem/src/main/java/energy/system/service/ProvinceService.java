package energy.system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.Province;

public interface ProvinceService {
	Province save(Province province);
	Province update(Province province);
	Province delete(long id);
	//Province getProvinceByAcronym(Province province, String acronym);
	

}
