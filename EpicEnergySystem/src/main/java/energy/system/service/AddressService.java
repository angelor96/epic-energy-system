package energy.system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.Address;

public interface AddressService {
	
	 Address save(String nameCity ,String acronym, long customerId,Address address);
	 Address update(String namceCity, String acronym, long customerId, Address address);
	 Address delete(long id);
	 Address getByWayAndCivicAndPlace( String way, int civic,String place);
	 Page<Address> getByCAP(int cap, Pageable pageable);
	
}
