package energy.system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.InvoiceStatus;

public interface InvoiceStatusService {
	
	 InvoiceStatus save(InvoiceStatus invoiceStatus);
	 InvoiceStatus update(InvoiceStatus invoiceStatus);
	 InvoiceStatus delete(long id);
	 Page<InvoiceStatus> getByInvoiceStatus(String invoiceStatus, Pageable pageable);

}
