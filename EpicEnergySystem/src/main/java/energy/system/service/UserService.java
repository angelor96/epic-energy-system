package energy.system.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.login.User;

public interface UserService {
	
	User save(long idRole,User user);
	User update(long idRole,User user);
	User delete(long id);
	Page<User> getByName(String name, Pageable pageable);
	Page<User> getBySurname(String surname, Pageable pageable);
	Optional<User> getByUsername(String username);
	User getByEmail(String email);
	void criptAllPasswords();	
	

}
