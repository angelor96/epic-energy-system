package energy.system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.City;
import energy.system.model.Province;

public interface CityService {
	
	City save(City city);
	City update(City city);
	City delete(long id);
	Page<Province> getProvinces(Pageable pageable);
	
	

}
