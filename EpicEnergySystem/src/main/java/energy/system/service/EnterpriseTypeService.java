package energy.system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.EnterpriseType;

public interface EnterpriseTypeService {
	
	EnterpriseType save(EnterpriseType enterpriseType);
	EnterpriseType update(EnterpriseType enterpriseType);
	EnterpriseType delete(long id);
	Page<EnterpriseType> getByEnterpriseType(EnterpriseType enterpriseType, Pageable pageable);

}
