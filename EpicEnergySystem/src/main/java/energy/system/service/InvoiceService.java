package energy.system.service;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.Enterprise;
import energy.system.model.Invoice;

public interface InvoiceService {
	
	 Invoice save(long entrpriseId, long invoiceStatusId, Invoice invoice);
	 Invoice update(long entrpriseId, long invoiceStatusId,Invoice invoice);
	 Invoice delete(long id);
	 Page<Invoice> getByYear(int year, Integer page, Integer size,String direction, String sort);
	 Page<Invoice> getByDate(Date date, Pageable pageable);
	 Page<Invoice> getByTotalBetween(BigDecimal total, BigDecimal total2,  Integer page, Integer size,String direction, String sort);
	 Invoice getByNumber(int number);
	 Page<Invoice> getAllByEnterpriseId( long id, Pageable pageable);
 	 Page<Invoice> getAllByOrderByDate(Integer page, Integer size,String direction, String sort);
 	 Page<Invoice> getByDateBetween(Date date, Date date2, Integer page, Integer size,String direction, String sort);
 	 Page<Invoice>  getInvoicesByOrderByEnterpriseId(long enterpriseId, Integer page, Integer size,String direction, String sort );
 	 Page<Invoice>  getInvoicesByOrderByInvoiceStatusId(long invoiceStatusId, Integer page, Integer size,String direction, String sort );
}
