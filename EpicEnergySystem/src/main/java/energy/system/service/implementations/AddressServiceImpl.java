package energy.system.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import energy.system.model.Address;
import energy.system.repository.AddressRepository;
import energy.system.repository.CityRepository;
import energy.system.repository.CustomerRepository;
import energy.system.repository.ProvinceRepository;
import energy.system.service.AddressService;
@Service
public class AddressServiceImpl implements AddressService{

	@Autowired
	AddressRepository addressRepo;
	
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	CityRepository cityRepo;
	
	@Autowired
	ProvinceRepository provinceRepo;
	@Override
	public Address save(String nameCity,String acronym, long customerId,Address address) {
		try {
			var province = provinceRepo.findByAcronym(acronym).get();
			var city=cityRepo.findByNameAndProvince(nameCity, province);
			address.setCity(city);
			var customer= customerRepo.findById(customerId);
			address.setCustomer(customer.get());
			
			return addressRepo.save(address);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Address update(String nameCity, String acronym, long customerId,Address address) {
		try {
			var province = provinceRepo.findByAcronym(acronym).get();
			var city=cityRepo.findByNameAndProvince(nameCity, province);
			address.setCity(city);
			var customer= customerRepo.findById(customerId);
			address.setCustomer(customer.get());
			Address a= addressRepo.findById(address.getId());
			if(address.getWay()!=null) {
				if(!address.getWay().isBlank()) {
				a.setWay(address.getWay());
			}
			}
			if(address.getCivic()!=0) {
				a.setCivic(address.getCivic());
			}
			if(address.getPlace()!=null) {
				if(!address.getPlace().isBlank()) {
				a.setPlace(address.getPlace());
				}
			}
			if(address.getCAP()!=0) {
				a.setCAP(address.getCAP());
			}
			return addressRepo.save(a);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Address delete(long id) {
		try {
			Address a= addressRepo.findById(id);
			addressRepo.delete(a);
			return a;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Address getByWayAndCivicAndPlace( String way, int civic,String place) {
		try {
			return addressRepo.findByWayAndCivicAndPlace( way, civic, place);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Page<Address> getByCAP(int cap, Pageable pageable) {
		try {
			
			return addressRepo.findByCAP(cap, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	
}
