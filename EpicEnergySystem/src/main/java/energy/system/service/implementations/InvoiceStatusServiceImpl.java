package energy.system.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import energy.system.model.InvoiceStatus;
import energy.system.repository.InvoiceStatusRepository;
import energy.system.service.InvoiceStatusService;

@Service
public class InvoiceStatusServiceImpl implements InvoiceStatusService{

	
	@Autowired
	InvoiceStatusRepository invoiceStatusRepo;
	
	@Override
	public InvoiceStatus save(InvoiceStatus invoiceStatus) {
		try {
			return invoiceStatusRepo.save(invoiceStatus);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public InvoiceStatus update(InvoiceStatus invoiceStatus) {
		try {
			InvoiceStatus i= invoiceStatusRepo.findById(invoiceStatus.getId());
			if(invoiceStatus.getInvoiceStatus()!=null) {
				if(!invoiceStatus.getInvoiceStatus().isBlank()) {
					i.setInvoiceStatus(invoiceStatus.getInvoiceStatus());
				}
			}
			return invoiceStatusRepo.save(i);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public InvoiceStatus delete(long id) {
		try {
			InvoiceStatus i= invoiceStatusRepo.findById(id);
			invoiceStatusRepo.delete(i);
			return i;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<InvoiceStatus> getByInvoiceStatus(String invoiceStatus, Pageable pageable) {
		try {
			return invoiceStatusRepo.findByInvoiceStatus(invoiceStatus, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
