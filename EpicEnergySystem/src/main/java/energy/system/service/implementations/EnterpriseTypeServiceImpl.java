package energy.system.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import energy.system.model.EnterpriseType;
import energy.system.repository.EnterpriseTypeRepository;
import energy.system.service.EnterpriseTypeService;
@Service
public class EnterpriseTypeServiceImpl implements EnterpriseTypeService{

	
	@Autowired
	EnterpriseTypeRepository enterpriseTypeRepo;
	
	@Override
	public EnterpriseType save(EnterpriseType enterpriseType) {
		try {
			return enterpriseTypeRepo.save(enterpriseType);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public EnterpriseType update(EnterpriseType enterpriseType) {
		try {
			EnterpriseType c= enterpriseTypeRepo.findById(enterpriseType.getId());
			if(enterpriseType.getEnterpriseType()!=null) {
				if(!enterpriseType.getEnterpriseType().isBlank()) {
					c.setEnterpriseType(enterpriseType.getEnterpriseType());
				}
			}
			return enterpriseTypeRepo.save(c);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public EnterpriseType delete(long id) {
		try {
			EnterpriseType c= enterpriseTypeRepo.findById(id);
			enterpriseTypeRepo.delete(c);
			return c;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<EnterpriseType> getByEnterpriseType(EnterpriseType enterpriseType, Pageable pageable) {
		try {
			return enterpriseTypeRepo.findByEnterpriseType(enterpriseType, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
