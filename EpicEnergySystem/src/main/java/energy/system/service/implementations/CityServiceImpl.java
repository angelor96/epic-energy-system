package energy.system.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import energy.system.model.City;
import energy.system.model.Province;
import energy.system.repository.CityRepository;
import energy.system.repository.ProvinceRepository;
import energy.system.service.CityService;
@Service
public class CityServiceImpl implements CityService{

	@Autowired
	CityRepository cityRepo;
	
	@Autowired
	ProvinceRepository provinceRepo;
	
	

	@Override
	public City save(City city) {
		try {
			Province province;
			// recupero la provincia dal database
			var p = provinceRepo.findByAcronym(city.getProvince().getAcronym());
				// se non c'e la inserisco
				if(p.isEmpty()) province = provinceRepo.save(city.getProvince());
				// altrimenti prendo quella recuperata
				else province= p.get();
			// alla fine metto il risultato della precedente istruzione nella provincia
			city.setProvince(province);
			return cityRepo.save(city);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public City update(City city) {
		try {
			City m=cityRepo.findById(city.getId());
			if(city.getName()!=null) {
				if(!city.getName().isBlank()) {
					m.setName(city.getName());
				}
			}
			return cityRepo.save(m);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public City delete(long id) {
		try {
			City m=cityRepo.findById(id);
			cityRepo.delete(m);
			return m;
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
    public Page<Province> getProvinces(Pageable pageable) {
        try {
            return provinceRepo.findAll(pageable);
        } catch (Exception e) {
            throw new RuntimeException(e);
    }

	}
	
	
}
