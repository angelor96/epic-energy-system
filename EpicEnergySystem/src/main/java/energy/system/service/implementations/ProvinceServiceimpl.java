package energy.system.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import energy.system.model.Province;
import energy.system.repository.ProvinceRepository;
import energy.system.service.ProvinceService;
@Service
public class ProvinceServiceimpl implements ProvinceService{

	@Autowired
	ProvinceRepository provinceRepo;
	@Override
	public Province save(Province province) {
		try {
			return provinceRepo.save(province);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Province update(Province province) {
		try {
			Province p= provinceRepo.findById(province.getId());
			if(province.getName()!=null) {
				if(!province.getName().isBlank()) {
					p.setName(province.getName());
				}
			}
			if(province.getAcronym()!=null) {
				if(!province.getAcronym().isBlank()) {
					p.setAcronym(province.getAcronym());
				}
			}
			return provinceRepo.save(p);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Province delete(long id) {
		try {
			Province p= provinceRepo.findById(id);
			provinceRepo.delete(p);
			return p;
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	

	

}
