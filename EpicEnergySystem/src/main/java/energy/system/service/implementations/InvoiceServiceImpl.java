package energy.system.service.implementations;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import energy.system.model.Enterprise;
import energy.system.model.Invoice;
import energy.system.repository.EnterpriseRepository;
import energy.system.repository.InvoiceRepository;
import energy.system.repository.InvoiceStatusRepository;
import energy.system.service.InvoiceService;
@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	InvoiceRepository invoiceRepo;
	
	@Autowired
	EnterpriseRepository enterpriseRepo;
	
	@Autowired
	InvoiceStatusRepository invoiceStatusRepo;
	
	@Override
	public Invoice save(long enterpriseId, long invoiceStatusId, Invoice invoice) {
		try {
			var enterprise=enterpriseRepo.findById(enterpriseId);
			invoice.setEnterprise(enterprise);
			var invoiceStatus=invoiceStatusRepo.findById(invoiceStatusId);
			invoice.setInvoiceStatus(invoiceStatus);
			return invoiceRepo.save(invoice);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Invoice update(long enterpriseId, long invoiceStatusId,Invoice invoice) {
		try {
			var enterprise=enterpriseRepo.findById(enterpriseId);
			invoice.setEnterprise(enterprise);
			var invoiceStatus=invoiceStatusRepo.findById(invoiceStatusId);
			invoice.setInvoiceStatus(invoiceStatus);
			Invoice i = invoiceRepo.findById(invoice.getId());
			if(invoice.getYear()!=0) {
				i.setYear(invoice.getYear());
			}
			if(invoice.getDate()!=null) {
					i.setDate(invoice.getDate());
				}
			if(invoice.getTotal()!=null) {
					i.setTotal(invoice.getTotal());
				}
			if(invoice.getNumber()!=0) {
				i.setNumber(invoice.getNumber());
			}
			return invoiceRepo.save(i);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Invoice delete(long id) {
		try {
			Invoice i = invoiceRepo.findById(id);
			invoiceRepo.delete(i);
			return i;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Page<Invoice> getByYear(int year, Integer page, Integer size,String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return invoiceRepo.findByYear(year, paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Page<Invoice> getByDate(Date date, Pageable pageable) {
		try {
			return invoiceRepo.findByDate(date, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Page<Invoice> getByTotalBetween(BigDecimal total,BigDecimal total2,Integer page, Integer size,String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return invoiceRepo.findByTotalBetween(total, total2, paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Invoice getByNumber(int number) {
		try {
			return invoiceRepo.findByNumber(number);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Invoice> getAllByEnterpriseId(long id, Pageable pageable) {
		try {
			return invoiceRepo.findAllByEnterpriseId( id, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Page<Invoice> getAllByOrderByDate(Integer page, Integer size, String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);
			return invoiceRepo.findAllByOrderByDate(paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	
	public Page<Invoice> getByDateBetween(Date date,Date date2,Integer page, Integer size,String direction, String sort) {
		try {
			
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return invoiceRepo.findByDateBetween(date, date2, paging);		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Invoice> getInvoicesByOrderByEnterpriseId(long enterpriseId, Integer page, Integer size, String direction,
			String sort) {
		try {
			var enterprise=enterpriseRepo.findById(enterpriseId);
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return invoiceRepo.findAllByEnterprise(enterprise, paging);		
			} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Invoice> getInvoicesByOrderByInvoiceStatusId(long invoiceStatusId, Integer page, Integer size,
			String direction, String sort) {
		try {
			var invoiceStatus=invoiceStatusRepo.findById(invoiceStatusId);
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return invoiceRepo.findAllByInvoiceStatus(invoiceStatus, paging);		
			} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	
	
	
}
