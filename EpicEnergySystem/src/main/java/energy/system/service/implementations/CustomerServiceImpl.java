package energy.system.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import energy.system.model.Customer;
import energy.system.repository.CustomerRepository;
import energy.system.service.CustomerService;
@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	CustomerRepository customerRepo;
	@Override
	public Customer save(Customer customer) {
		try {
			return customerRepo.save(customer);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Customer update(Customer customer) {
		try {
			Customer c= customerRepo.findById(customer.getId()).get();
			if(customer.getName()!=null) {
				if(!customer.getName().isBlank()) {
					c.setName(customer.getName());
				}
			}
			if(customer.getSurname()!=null) {
				if(!customer.getSurname().isBlank()) {
					c.setSurname(customer.getSurname());
				}
			}
			if(customer.getEmail()!=null) {
				if(!customer.getEmail().isBlank()) {
					c.setEmail(customer.getEmail());
				}
			}
			if(customer.getTelephone()!=null) {
					c.setTelephone(customer.getTelephone());
			}
			return customerRepo.save(c);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		
	}

	@Override
	public Customer delete(long id) {
		try {
			Customer c= customerRepo.findById(id).get();
			customerRepo.delete(c);
			return c;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<Customer> getByName(String name, Pageable pageable) {
		try {
			return customerRepo.findByName(name, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
			}
		
	}

	@Override
	public Page<Customer> getBySurname(String surname, Pageable pageable) {
		try {
			return customerRepo.findBySurname(surname, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Customer getByEmail(String email) {
		try {
			return customerRepo.findByEmail(email);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Customer getByTelephone(String telephone) {
		try {
			return customerRepo.findByTelephone(telephone);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

}
