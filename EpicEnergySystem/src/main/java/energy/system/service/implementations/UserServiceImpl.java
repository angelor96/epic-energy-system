package energy.system.service.implementations;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import energy.system.model.login.User;
import energy.system.repository.RoleRepository;
import energy.system.repository.UserRepository;
import energy.system.service.UserService;
@Service
public class UserServiceImpl implements UserService {

	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Override
	public User save(long idRole, User user) {
		try {
			String convertPassword= encoder.encode(user.getPassword());
			user.setPassword(convertPassword);
			var role=roleRepo.findById(idRole);
			user.getRole().add(role);
			return userRepo.save(user);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public User update(long idRole,User user) {
		try {
			var role=roleRepo.findById(idRole);
			user.getRole().add(role);
			User u= userRepo.findById(user.getId());
			if(user.getName()!=null) {
				if(!user.getName().isBlank()) {
					u.setName(user.getName());
				}
			}
			if(user.getSurname()!=null) {
				if(!user.getSurname().isBlank()) {
					u.setSurname(user.getSurname());
				}
			}
			if(user.getEmail()!=null) {
				if(!user.getEmail().isBlank()) {
					u.setEmail(user.getEmail());
				}
			}
			if(user.getPassword()!=null) {
				if(!user.getPassword().isBlank()) {
					u.setPassword(user.getPassword());
				}
			}
			if(user.getRole()!=null) {
					u.setRole(user.getRole());
				}
			
			return userRepo.save(u);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public User delete(long id) {
		try {
			User u= userRepo.findById(id);
			u.getRole().clear();
			userRepo.delete(u);
			return u;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<User> getByName(String name, Pageable pageable) {
		try {
			return userRepo.findByName(name, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Page<User> getBySurname(String surname, Pageable pageable) {
		try {
			return userRepo.findBySurname(surname, pageable);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<User> getByUsername(String username) {
		try {
			return userRepo.findByUsername(username);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public User getByEmail(String email) {
		try {
			return userRepo.findByEmail(email);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void criptAllPasswords(){
		
		List<User> list = userRepo.findAll();
		
		for(User u : list) {
			String hashedPassword = encoder.encode(u.getPassword());
			u.setPassword(hashedPassword);
			userRepo.save(u);
		}
		
	}

}
