package energy.system.service.implementations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import energy.system.model.Enterprise;
import energy.system.model.Invoice;
import energy.system.repository.CustomerRepository;
import energy.system.repository.EnterpriseRepository;
import energy.system.repository.EnterpriseRepository.ImportPerYear;
import energy.system.repository.EnterpriseTypeRepository;
import energy.system.service.EnterpriseService;
@Service
public class EnterpriseServiceImpl implements EnterpriseService{

	
	@Autowired
	EnterpriseRepository enterpriseRepo;
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	EnterpriseTypeRepository enterpriseTypeRepo;
	
	@Override
	public Enterprise save(long customerId,long enterpriseTypeId, Enterprise enterprise) {

		
		try {
			var customer= customerRepo.findById(customerId);
			var enterpriseType=enterpriseTypeRepo.findById(enterpriseTypeId);
			enterprise.setCustomer(customer.get());
			enterprise.setEnterpriseType(enterpriseType);
			return enterpriseRepo.save(enterprise);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Enterprise update(long customerId, long enterpriseTypeId,Enterprise enterprise) {
		try {
			var customer= customerRepo.findById(customerId);
			var enterpriseType=enterpriseTypeRepo.findById(enterpriseTypeId);
			enterprise.setCustomer(customer.get());
			enterprise.setEnterpriseType(enterpriseType);
			Enterprise e=enterpriseRepo.findById(enterprise.getId());
			if(enterprise.getName()!=null) {
				if(!enterprise.getName().isBlank()) {
					e.setName(enterprise.getName());
				}
			}
			if(enterprise.getVAT()!=null) {
				if(!enterprise.getVAT().isBlank()) {
					e.setVAT(enterprise.getVAT());
				}
			}
			if(enterprise.getEmail()!=null) {
				if(!enterprise.getEmail().isBlank()) {
					e.setEmail(enterprise.getEmail());
				}
			}
			if(enterprise.getInsertDate()!=null) {
				e.setInsertDate(enterprise.getInsertDate());
				}
			
			if(enterprise.getUltimateTouch()!=null) {
				e.setUltimateTouch(enterprise.getUltimateTouch());
				}
			
			if(enterprise.getPEC()!=null) {
				if(enterprise.getPEC().isBlank()) {
					e.setPEC(enterprise.getPEC());
				}
			}
			if(enterprise.getTelephone()!=null) {
				if(!enterprise.getTelephone().isBlank()) {
					e.setTelephone(enterprise.getTelephone());
				}
			}
			return enterpriseRepo.save(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Enterprise delete(long id) {
		try {
			Enterprise e=enterpriseRepo.findById(id);
			enterpriseRepo.delete(e);
			return e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Enterprise getByName(String name) {
		try {
			return enterpriseRepo.findByName(name);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Enterprise getByVAT(String VAT) {
		try {
			return  enterpriseRepo.findByVAT(VAT);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Enterprise getByEmail(String email) {
		try {
			return  enterpriseRepo.findByEmail(email);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Page<Enterprise> getByInsertDateBetween(LocalDate insertDate, LocalDate insertDate2,  Integer page, Integer size,String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return  enterpriseRepo.findByInsertDateBetween(insertDate,insertDate2, paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Page<Enterprise> getByUltimateTouchBetween(Date ultimateTouch, Date ultimateTouch2,Integer page, Integer size,String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);
			return  enterpriseRepo.findByUltimateTouchBetween(ultimateTouch,ultimateTouch2, paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	

	@Override
	public Enterprise getByPEC(String PEC) {
		try {
			return  enterpriseRepo.findByPEC(PEC);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Enterprise getByTelephone(String telephone) {
		try {
			return  enterpriseRepo.findByTelephone(telephone);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	

	@Override
	public Page<Enterprise> getAllOrderedByName( Integer page, Integer size, String direction,	String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return enterpriseRepo.findAllByOrderByName( paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public Page<Enterprise> getAllByOrderByInsertDate(Integer page, Integer size, String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);
			
			return enterpriseRepo.findAllByOrderByInsertDate(paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public BigDecimal getImportPerYear(long enterpriseId, int year) {
		try {
			return enterpriseRepo.findImportByYear(enterpriseId, year);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		
	}

	public Page<Enterprise> getAllOrderedByUltimateTouch(Integer page, Integer size, String direction, String sort) {
		try {
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);
			
			return enterpriseRepo.findAllByOrderByUltimateTouch(paging);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<ImportPerYear> getTotalByYear(int year) {
		
		try {
			
			return enterpriseRepo.findTotalByYear(year);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	
	@Override
	public Page<Enterprise> getByNameContains(String name, Integer page, Integer size, String direction, String sort) {
		try {
			
			Pageable paging= PageRequest.of(page, size,Sort.Direction.fromString(direction),sort);

			return enterpriseRepo.findByNameContains(name, paging);	
			} 
			catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	

	

	
	
	

	

	
		
	

}
