package energy.system.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import energy.system.model.Enterprise;
import energy.system.model.Invoice;
import energy.system.repository.EnterpriseRepository.ImportPerYear;

public interface EnterpriseService {
	
	 Enterprise save(long customerId,long enterpriseTypeId, Enterprise enterprise);
	 Enterprise update(long customerId, long enterpriseTypeId,Enterprise enterprise);
	 Enterprise delete(long id);
	 Enterprise getByName(String name);
	 Enterprise getByVAT(String VAT);
	 Enterprise getByEmail(String email);
	 Enterprise getByPEC(String PEC);
	 Enterprise getByTelephone(String telephone);
	 Page<Enterprise> getByInsertDateBetween(LocalDate insertDate, LocalDate insertDate2,Integer page, Integer size,String direction, String sort);
	 Page<Enterprise> getByUltimateTouchBetween(Date ultimateTouch, Date ultimateTouch2, Integer page, Integer size,String direction, String sort);
 	 Page<Enterprise> getAllOrderedByName( Integer page, Integer size,String direction, String sort);
 	 Page<Enterprise> getAllByOrderByInsertDate(Integer page, Integer size,String direction, String sort);
 	 BigDecimal getImportPerYear(long enterpriseId, int year);
 	 Page<Enterprise> getAllOrderedByUltimateTouch(Integer page, Integer size, String direction, String sort);

 	List<ImportPerYear> getTotalByYear(int year);
 	
	 Page<Enterprise> getByNameContains(String name,  Integer page, Integer size,String direction, String sort);
	 

}
