package energy.system.runner;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import be06.epicode.cities.CitiesLoader;
import be06.epicode.cities.models.MyCity;
import be06.epicode.cities.models.MyProvince;
import energy.system.model.City;
import energy.system.model.Enterprise;
import energy.system.model.Province;
import energy.system.repository.ProvinceRepository;
import energy.system.service.EnterpriseService;
import energy.system.service.implementations.CityServiceImpl;
import energy.system.service.implementations.ProvinceServiceimpl;

@Component
public class Program implements CommandLineRunner {

	@Autowired
	EnterpriseService eService;

	@Autowired
	CityServiceImpl cityServiceImpl;
	
	@Autowired
	ProvinceRepository provinceRepo;
	
	@Autowired
	CityServiceImpl cService;
	
	@Autowired
	ProvinceServiceimpl pService;
	
	
	

	@Override
	public void run(String... args) throws Exception {
		
		var enterprise= Enterprise.builder()
				.name("Eraso")
				.VAT("0018534235")
				.email("erasoclothing@libero.it")
				
				.build();
		
		//eService.save(enterprise);
		
		/*
		 * 
		 * Metodo di caricamento delle città e delle province.
		 * 
		 */
		String fileName = "C:\\Users\\39327\\git\\epicenergysystem\\EpicEnergySystem\\Elenco-comuni-italiani.csv";

		Set<MyCity> cities = CitiesLoader.load(new FileInputStream(fileName));
		cities.stream()
				.map(c -> City.builder().capital(c.isCapital()).name(c.getName())
						.province(Province.builder().name(c.getProvince().getName())
								.acronym(c.getProvince().getAcronym()).build())

						.build())
				
				//.forEach(c -> cService.save(c))
				;
		
							
							
							
							
							
							
							
							
	}
	
	
	}
	

	
	
	
	
	
	

	
