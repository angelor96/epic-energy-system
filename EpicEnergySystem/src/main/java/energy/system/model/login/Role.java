package energy.system.model.login;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import energy.system.model.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
public class Role extends BaseEntity{
    
	
	    @Enumerated(EnumType.STRING)
	    private RoleType roleType;
	 
	    public Role(RoleType roleType) {
	        this.roleType = roleType;
	    }
	    
	    @Override
	    public String toString() {
	    	
	        return String.format("Role:  roletype%s ",  roleType);
	        }
	    

	    
}

