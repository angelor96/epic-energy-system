package energy.system.model.login;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import energy.system.security.Converter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Entity
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User{
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(nullable = false, length = 125)
	private String name;
	@Column(nullable = false, length = 125)
	private String surname;
	@Column(nullable = false, length = 125, unique=true)
	private String username;
	@Column(nullable = false, length = 125, unique=true)
	//@Convert(converter = Converter.class)
	private String email;
	@Column(nullable = false, length = 125)
	private String password;
	@Column(nullable = false, length = 125)
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinTable(
	            name="user_role",
	            joinColumns= @JoinColumn(name="user_id", referencedColumnName="id"),
	            inverseJoinColumns= @JoinColumn(name="role_id", referencedColumnName="id")
	        )
		private Set<Role> role = new HashSet<>();

}
