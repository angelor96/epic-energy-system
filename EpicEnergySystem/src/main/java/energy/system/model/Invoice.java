package energy.system.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Entity
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Invoice extends BaseEntity{
	
	private int year;
	
	private Date date;
	private BigDecimal total;
	private int number;
	/*
	 * Azienda alla quale le fatture fanno riferimento
	 */
	@ManyToOne(cascade = CascadeType.DETACH)
	@JsonIgnoreProperties({"invoices"})
	private Enterprise enterprise;
	/*
	 * Stato fattura di riferimento alla medesima
	 */
	@ManyToOne(cascade = CascadeType.DETACH)
	private InvoiceStatus invoiceStatus;
	
	// 

}
