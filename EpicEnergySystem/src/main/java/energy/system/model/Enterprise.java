package energy.system.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Enterprise extends BaseEntity{
	
	private String name;
	private String VAT;
	private String email;
	private LocalDate insertDate;
	@Column(
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", 
            insertable = false, 
            updatable = false)
	private Date ultimateTouch;		
	private String PEC;
	private String telephone;
	@Enumerated(EnumType.STRING)
	private Office office;
	/**
	 * Persona alla quale l'azienda fa riferimento
	 */
	@OneToOne(cascade = CascadeType.DETACH)
	private Customer customer;
	/**
	 * Fatture collegate all'azienda
	 */
	@OneToMany(mappedBy = "enterprise",fetch = FetchType.LAZY)
	private List<Invoice> invoices;
	/*
	 * Tipologia di azienda collegata
	 */
	@ManyToOne
	private EnterpriseType enterpriseType;
	
	

}
