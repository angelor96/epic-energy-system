package energy.system.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Entity
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Address extends BaseEntity{

	
	private String way;
	private int civic;
	private String place;
	private int CAP;
	
	/**
	 * Città alla quale l'indirizzo fa riferimento
	 */
	@ManyToOne
	private City city;
	
	/**
	 *  Cliente a cui l'indirizzo fa riferimento
	 */
	@ManyToOne
	private Customer customer;
	

}
