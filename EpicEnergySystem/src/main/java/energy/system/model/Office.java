package energy.system.model;

public enum Office {
	
	REGISTERED_OFFICE, OPERATIONAL_HEADQUARTERS

}
