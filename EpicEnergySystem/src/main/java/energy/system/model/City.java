package energy.system.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Entity
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class City extends BaseEntity{
	
	private String name;
	private boolean capital;
	
	/*
	 * Provincia alla quale la città fa riferimento
	 */
	@ManyToOne
	private Province province;
	

}
